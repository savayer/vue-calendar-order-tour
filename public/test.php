<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<html lang=en>

<head>
  <meta charset=utf-8>
  <meta http-equiv=X-UA-Compatible content="IE=edge">
  <meta name=viewport content="width=device-width,initial-scale=1">  
  <title>order-tour</title>
  <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/app.1f704596.css" rel=preload as=style>
  <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/app.590b45dc.js" rel=preload as=script>

  <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/chunk-vendors.da6c2354.js" rel=preload as=script>
  <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/app.1f704596.css" rel=stylesheet>
  
</head>

<body>
  <?php
  	if ($_GET['id']) {
		$db = &JFactory::getDBO();
        $sql = "SELECT * FROM j_tickets_tours_list WHERE id = '" . $_GET['id'] . "'";    
        $db->setQuery($sql);
        $db->query();
        $data_rows_assoc_list = $db->loadAssocList();
        echo '<script>';
        echo 'var tour = ' . json_encode($data_rows_assoc_list) . ';';
        echo 'var db = JSON.parse(tour[0].data);';
        echo '</script>';    
    }

  ?>
  <div class=wrapper>
    <header><a href=/><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo.png"" alt=logo class=logo></a></header>
    <section class=steps>
      <div class="step step1">
        <div class=number><span>1</span></div>
        <div class=text>ךלש לויטה תא רחב</div>
      </div>
      <div class="step step2 current-step">
        <div class=number><span>2</span></div>
        <div class=text>האיצי ךיראת רחב</div>
      </div>
      <div class="step step3">
        <div class=number><span>3</span></div>
        <div class=text>3 דעצ</div>
      </div>
      <div class="step step4">
        <div class=number><span>4</span></div>
        <div class=text>4 דעצ</div>
      </div>
    </section>
    <h1 class=head>האיצי ךיראת רחב</h1>
  </div>
  <div id=app></div>
  <div class=wrapper>
    <section class=information dir=rtl>
      <h3>המשרהל בושח עדימ</h3>
      <p>רושיא לבקל שי רחא ףוסיא םוקמ לכל ,דבלב המנפיאו הנבקהפוק תונוכשב םיישארה םיריצה ךרואל םיעצבתמ ונלש םילויטל
        םיפוסיאה הנילה םוקממ רצק הכילה קחרמב דימת םה ףוסיאה תודוקנ.לויטל האיציל םכל רסמיתש ףוסיאה תדוקנל עיגהל שי
        .ינטרפ .הנבקהפוק וא המנפיאב</p>
      <p>לויטה ינפל םוי ברעב 00:20 דע קיודמ םוקמו ףוסיא תעש תא םיעידומ ונא - יתבוהא ויר<br>לויטל האיציה םויב 00:12 העשה
        דע םילבקתמ קיודמ םוקמו ףוסיאה תעש - הבמסה ריע ויר</p>
      <p>המשרה רושיא חלשיי ,םולשתה רושיאו המשרהה רחאל</p>
      <p>לויטל האיציה ינפל תועש 24 דע לוטיבל םינתינ לאיר - 50 המשרהה ימד<br>לויטל האיציה ינפל םוי ,ברעב 00:20 העשה דע
        םולשתב ךורכ וניא האיציה ךיראת יוניש</p>
    </section>
    <footer><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/footer-logo.png"" alt="footer logo"></footer>
  </div>
  <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/chunk-vendors.da6c2354.js"> </script> 
  <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/app.590b45dc.js"> </script> 

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
   <script>
   		$(document).ready(function() {
            var tour = window.tour[0];
            console.log(tour);
    		var td = $('#calendar td');
    
    		$(td).on('click', function() {
              $(td).removeClass('active-date');
              
              $(this).addClass('active-date');
            })

            $('.order .image img').attr('src', '/' + tour.tour_image)
            $('.order .title b').text(tour.tour_title)
  		}) 
   </script>
</body> 
</html>